var express = require('express');
var router = express.Router();

/* GET home page. */

module.exports = router;

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/about', function(req, res, next) {


    let selectOptions = [
        {
            value: 1, name: "option1"
        },{
            value: 2, name: "option2"
        },{
            value: 3, name: "option3"
        },{
            value: 4, name: "option4"
        },
        ];

    res.render('about', {options: selectOptions});
});

router.get(
    '/light',
    function (req, res) {

        res.render('light-gal');
    }
);


